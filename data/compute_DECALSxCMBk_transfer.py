import healpy as hp
import pymaster as nmt
from cobaya.model import get_model
import cl_like as cll
import numpy as np
from matplotlib import pyplot as plt
import os

# Rotator for Planck
rot = hp.Rotator(coord=['G', 'C'])

# Paths to the input and reconstructed kappa maps
# The maps in rec_sims_path are called sim_klm_XXX.fits ranging from 000 to 299.
# The maps in input_sims_path are called sky_klm_XXX.fits ranging from 000 to 299.
rec_sims_path = '/mnt/extraspace/vonhausegger/Datasets/Planck_lensing/COM_Lensing-SimMap_4096_R3.00'
input_sims_path = '/mnt/extraspace/vonhausegger/Datasets/Planck_lensing/COM_Lensing-SimMap-inputs_4096_R3.00/'

# Masks
print("Reading masks", flush=True)
mask_decals = hp.read_map("/mnt/extraspace/damonge/Datasets/DELS/xcell_reruns/mask_mask_DELS_decm36_coordC_ns4096.fits.gz")
mask_planck = hp.read_map("/mnt/extraspace/damonge/Datasets/Planck_lensing/Lensing2018/xcell_runs/mask_mask_P18kappa_coordC_ns4096.fits.gz")

# MASTER stuff
bpw_edges = [0, 30, 60, 90, 120, 150, 180, 210, 240, 272, 309, 351, 398, 452, 513, 582, 661, 750, 852, 967, 1098, 1247, 1416, 1608, 1826, 2073, 2354, 2673, 3072, 3*4096]
b = nmt.NmtBin.from_edges(bpw_edges[:-1], bpw_edges[1:])
ell_eff = b.get_effective_ells()

# Read workspace
w = nmt.NmtWorkspace()
fname = "/mnt/users/gravityls_3/codes/coevolution-in-data/data/w_mask_DELS_decm36_mask_P18kappa.fits"
if os.path.isfile(fname):
    print("Reading NmtWorkspace", flush=True)
    w.read_from(fname)
else:
    f1 = nmt.NmtField(mask_decals, None, spin=0)
    f2 = nmt.NmtField(mask_planck, None, spin=0)

    print("Generating NmtWorkspace", flush=True)
    w.compute_coupling_matrix(f1, f2, b)
    print("Saving NmtWorkspace", flush=True)
    w.write_to(fname)

Tl = np.zeros((300, b.get_n_bands()))
# Loop over all
for i in range(300):
    fname = f'/mnt/users/gravityls_3/codes/coevolution-in-data/data/transfer_DECALSxCMBk/transfer_DECALSxCMBk_{i}.npz'
    if os.path.isfile(fname):
        print(f"Reading Tl {i+1}/300", flush=True)
        Tlfile = np.load(fname)
        Tl[i] = Tlfile['Tl']
    else:
        print(f"Computing Tl {i+1}/300", flush=True)
        kappa_rec_alm = rot.rotate_alm(hp.read_alm(rec_sims_path + '/' + f'sim_klm_{i:03d}.fits'))
        kappa_input_alm = rot.rotate_alm(hp.read_alm(input_sims_path + '/' + f'sky_klm_{i:03d}.fits'))

        kappa_input_map = hp.alm2map(kappa_input_alm, 4096)
        kappa_rec_map = hp.alm2map(kappa_rec_alm, 4096)

        kin_gmask = nmt.NmtField(mask_decals, [kappa_input_map])  # K_in,g-mask
        krec = nmt.NmtField(mask_planck, [kappa_rec_map])  # K_rec
        kin_kmask = nmt.NmtField(mask_planck, [kappa_input_map]) # K_in,k-mask

        cl_kin_kmask__kin_gmask_cp = nmt.compute_coupled_cell(kin_kmask, kin_gmask)
        cl_kin_kmask__kin_gmask = w.decouple_cell(cl_kin_kmask__kin_gmask_cp)

        cl_krec__kin_gmask_cp = nmt.compute_coupled_cell(krec, kin_gmask)
        cl_krec__kin_gmask = w.decouple_cell(cl_krec__kin_gmask_cp)

        Tl[i] = cl_kin_kmask__kin_gmask[0]/cl_krec__kin_gmask[0]

        print(f"Saving {fname}", flush=True)
        np.savez_compressed(fname,
                           ell=ell_eff,
                           ell_cp=np.arange(bpw_edges[-1]),
                           #
                           cl_kin_kmask__kin_gmask_cp=cl_kin_kmask__kin_gmask_cp,
                           cl_krec__kin_gmask_cp=cl_krec__kin_gmask_cp,
                           #
                           cl_kin_kmask__kin_gmask=cl_kin_kmask__kin_gmask,
                           cl_krec__kin_gmask=cl_krec__kin_gmask,
                           Tl=Tl[i])

Tlmean = np.mean(Tl, axis=0)
fname = '/mnt/users/gravityls_3/codes/coevolution-in-data/data/transfer_DECALSxCMBk/transfer_DECALSxCMBk.npz'
print(f"Saving Tl and Tlmean to {fname}", flush=True)
np.savez_compressed(fname, Tlmean=Tlmean, Tl=Tl, ell=ell_eff)

print("Plotting Tlmean", flush=True)
plt.errorbar(b.get_effective_ells(), Tlmean, fmt='.')
plt.semilogx()
plt.xlabel("$\ell$")
plt.ylabel("$T_\ell$")
plt.title("DECALS x CMBk")
plt.savefig("/mnt/users/gravityls_3/codes/coevolution-in-data/data/transfer_DECALSxCMBk/transfer_DECALSxCMBk.pdf")
plt.show()
plt.close()
